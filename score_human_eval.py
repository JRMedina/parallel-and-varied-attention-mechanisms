import json
import copy
import numpy as np
from scipy.stats.stats import pearsonr
from scipy.stats import normaltest

import NeuralSum as ns

"""
Load all sentnece summary pairs from sentence_summary_pairs.txt
score each with VERT. Use same ordering as human evaluations.
output to json file so we can load it easily for comparing with
    human evaluators.
json file: human_scores.json
Note:
    the experiment included a duplicate article. The sentence of
        this article contains: 'organizers of december 's asian games have'
    we are not removing this duplicate.
"""

def load(f_name):
    sentences = []
    summaries = []

    was_sentence = False
    was_summary = False

    with open(f_name, 'r') as f:
        for line in f:
            if was_sentence:
                was_sentence, was_summary = False, False
                sentences.append(line.strip())
            elif was_summary:
                was_sentence, was_summary = False, False
                summaries.append(line.strip())
            elif line.strip() == 'Sentence:':
                was_sentence, was_summary = True, False
            elif line.strip() == 'Summary:':
                was_sentence, was_summary = False, True
            else:
                pass

    assert(len(sentences) == len(summaries))
    return sentences, summaries

def filter_articles(articles, sentences):
    # filter out articles that do not include entries from sentences, summaries
    new_articles = []
    for art in articles:
        if art.sentence in sentences:
            # ensure article is not already in new_articles
            skip = False
            for new_art in new_articles:
                if art.sentence == new_art.sentence:
                    skip = True
                    break
            if not skip:
                new_articles.append(art)

    # add in accidental test duplicate
    dup_art = None
    for art in new_articles:
        if "organizers of december 's asian" in art.sentence:
            dup_art = copy.deepcopy(art)
            break

    if dup_art == None:
        assert False, 'dup article not found'
    new_articles.append(dup_art)
    assert(len(new_articles) == len(sentences))
    return new_articles

def add_summaries(articles, summaries, sentences):
    # sorting of articles and summaries do not have to be the same
    for art in articles:
        summ_index = sentences.index(art.sentence)
        art.generated_summary = summaries[summ_index]
    return articles

def sort_articles_by_sentence(articles, sentences):
    # has same order as the human evals
    new_arts = []
    for sent in sentences:
        for art in articles:
            if art.sentence == sent:
                new_arts.append(art)
                break

    return new_arts

def score_vert():
    articles = ns.parse_duc_2004()
    sentences, summaries = load('./sentence_summary_pairs.txt')
    articles = filter_articles(articles, sentences)
    articles = add_summaries(articles, summaries, sentences)
    articles = sort_articles_by_sentence(articles, sentences)

    vert = ns.Vert(save_memory=False)
    all_scores = list(map(
        lambda art:
        vert.score_from_articles(
            [art],
            rouge_type='recall',
            verbose=True
        ),
        articles
    ))

    with open('human_scores.json', 'w') as f:
        json.dump(all_scores, f, indent=2)

def load_vert(f_name):
    with open(f_name) as f:
        all_scores = json.load(f)
    return all_scores

def load_human_eval(f_name):
    """
    Return a list of evaluations with order preserved.
    Evals will be lists of 5 themselves.

    Responsiveness:1
    Grammaticality:5
    Non-redundancy:5
    Referential Clarity:5
    Structure and Coherence:5
    """
    evals = []
    with open(f_name, 'r') as f:
        single_eval = {}
        for line in f:
            if 'Responsiveness' in line:
                single_eval['responsiveness'] = int(line.strip()[-1])
            elif 'Grammaticality' in line:
                single_eval['grammaticality'] = int(line.strip()[-1])
            elif 'Non-redundancy' in line:
                single_eval['non-redundancy'] = int(line.strip()[-1])
            elif 'Referential Clarity' in line:
                single_eval['clarity'] = int(line.strip()[-1])
            elif 'Structure and Coherence' in line:
                single_eval['structure'] = int(line.strip()[-1])
                evals.append(single_eval)
                single_eval = {}
            else:
                pass

    return evals

def avg_responsiveness(h1, h2):
    """
    Returns a list of averaged responsiveness
     where the index is the pair number - 1.
    """
    evals = []
    r1 = list(map(lambda e: e['responsiveness'], h1))
    r2 = list(map(lambda e: e['responsiveness'], h2))
    for resp_1, resp_2 in zip(r1,r2):
        evals.append(np.mean(np.array([resp_1, resp_2])))
    return evals

def avg_all(h1, h2):
    """
    average the two human evaluations over every question.
    """
    evals = []
    for e1, e2 in zip(h1, h2):
        evals.append((np.mean(np.array([sum(e1.values()),sum(e2.values())]))))
    return evals

def score_pearson():
    """
    Problem: vert doesnt seem normally distributed.
    Problem: high p-values due to small sample size
    """
    vert_eval = load_vert('human_scores.json')
    human_eval_1 = load_human_eval('paul_evals.txt')
    # human_eval_2 = load_human_eval('andrew_evals.txt')
    human_avg = np.array(avg_responsiveness(human_eval_1, human_eval_1))
    human_avg = avg_all(human_eval_1, human_eval_1)
    print 'Pearson Correlations (correlation, p-value)'
    print 'VERT:\t\t', pearsonr(
        np.array(map(lambda e: float(e['vert_score']), vert_eval)),human_avg)
    print 'rouge-1:\t', pearsonr(
        np.array(map(lambda e: float(e['rouge-1']), vert_eval)),human_avg)
    print 'rouge-2:\t', pearsonr(
        np.array(map(lambda e: float(e['rouge-2']), vert_eval)),human_avg)
    print 'rouge-l:\t', pearsonr(
        np.array(map(lambda e: float(e['rouge-l']), vert_eval)),human_avg)

    print 'VERT normal test:', normaltest(map(lambda e: float(e['vert_score']), vert_eval))

if __name__ == '__main__':
    # score_vert()
    score_pearson()
